﻿using System;
using TestXF.ViewModels;
using Xamarin.Forms;

namespace TestXF
{
    public partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel();
            
            MyButton.Clicked += Add;
        }
        
        private void Add(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(MyButton.Text))
            {
                ((Button) sender).Text = "1";
            }
            else
            {
                var act = Int32.Parse(MyButton.Text) + 1;
                ((Button) sender).Text = act.ToString();
            }
            
            MyLabel.Text = MyButton.Text;
        }
    }
}