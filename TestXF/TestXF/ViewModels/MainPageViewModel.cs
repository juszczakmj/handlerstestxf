using System.Windows.Input;
using Xamarin.Forms;

namespace TestXF.ViewModels
{
    public class MainPageViewModel
    {
        private ExtendedBindableObject _extendedBindableObject;
        public MainPageViewModel()
        {
            IncreaseLabelCounter = new Command(IncreaseLabelValue);
        }

        private int _labelValue = 0;
        public int LabelValue
        {
            get => _labelValue;
            set
            {
                _labelValue = value;
                 _extendedBindableObject.RaisePropertyChanged(() => LabelValue);
            }
        }

        public ICommand IncreaseLabelCounter { get; set; }

        private void IncreaseLabelValue()
        {
            _labelValue += 10;
        }
    }
}